# Figures

This directory contains the notebooks that have been used to produce the figures in the manuscript.

The figures have been edited and assembled using Inkscape.

