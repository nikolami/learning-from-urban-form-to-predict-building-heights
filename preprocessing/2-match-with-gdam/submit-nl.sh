#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=match_w_gdam-%A_%a
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --output=match_w_gdam_out-%A_%a.txt
#SBATCH --error=match_w_gdam_err-%A_%a.txt
#SBATCH --array=1-12


pwd; hostname; date

module load anaconda

source activate urban_form

python -u match_with_gdam_Netherlands.py i $SLURM_ARRAY_TASK_ID
