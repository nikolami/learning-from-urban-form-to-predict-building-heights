import pandas as pd
import geopandas as gpd
from shapely import wkt
import os,sys

base_path = '/data/metab/osm/ML_paper'
path_root = os.path.join(base_path,'git-plos-paper')
path_region_inputs = '/home/nikolami/ML_paper/Data/1-data_primary/3d_models/regions/'
path_output_dir = os.path.join(base_path,'Data','2-data_preprocessed','Netherlands')

sys.path.append(path_root)
from utils.preproc_utils import retrieve_city_boundary_gdf, city_boundary_plus_buffers_gdf_wkt
from utils.helpers import import_csv_w_wkt_to_gdf

crs = 28992
GDAM_level = 'NAME_2'

# match region names to jobs in SLURM array
region_names = { 1:'Drenthe',
2:'Flevoland',
3:'Friesland',
4:'Gelderland',
5:'Groningen',
6:'Limburg',
7:'Noord-Brabant',
8:'Noord-Holland',
9:'Overijssel',
10:'Utrecht',
11:'Zeeland',
12:'Zuid-Holland',
}

# intiatialize an argument parser
parser = argparse.ArgumentParser()
# create an argument i for the index of city 
parser.add_argument('-i', help="SLURM_ARRAY_TASK_ID", type=int)
# store in a variable
args = parser.parse_args()

region_name = region_names[args.i]
path_region = os.path.join(path_region_inputs,region_name)

print(args.i)
print(region_name)

## Set-up

# import gdam
GDAM_file = gpd.read_file('/home/nikolami/ML_paper/Data/1-data_primary/GADM/Netherlands/gadm36_NLD_2.shp')
GDAM_file = gdam_nl.to_crs(crs)

# itialize counter
city_counts = pd.DataFrame(columns = ['region', 'city', 'count'])

# create region folder in preproc data directory
os.makedirs(os.path.join(path_output_dir,region_name), exist_ok=True)

# import region files
region = import_csv_w_wkt_to_gdf(path_region,crs)

# change the name of the city column
region = region.rename(columns = {'city':'city_cadaster'})

# filter cities in region
city_names = list(GDAM_file[GDAM_file.NAME_1==region_name][GDAM_level])


## Matching
    
for city_name in city_names:
    
    print(city_name)
    
    ## do Buildings
    
    # get boundary
    city_boundary_gdf = retrieve_city_boundary_gdf(city_name, GDAM_file, GDAM_level, crs)

    # get buildings in boundary
    city_gdf = gpd.sjoin(region,city_boundary_gdf, how='inner',op='intersects')
    city_gdf = city_gdf.drop(columns=['index_right'])
    print(len(city_gdf))

    # remove buildings from region
    region = region.drop(city_gdf.index)
    
    # store counts
    city_counts = city_counts.append(pd.DataFrame({'region':departement_name,
                                                    'city':city_name,
                                                    'count':len(city_gdf)},index = [0]))
    ## do Boundaries w/ buffers
    
    city_boundary_gdf = city_boundary_plus_buffers_gdf_wkt(city_name, GDAM_file, GDAM_level, crs)


    ## Save

    if len(city_gdf) > 0:

        path_city_dir = os.path.join(path_output_dir,region_name,city_name)
        os.makedirs(path_city_dir, exist_ok=True)

       # boundaries
        city_gdf['geometry'] = city_gdf['geometry'].apply(lambda x: x.wkt)
        city_gdf = pd.DataFrame(city_gdf).reset_index(drop=True)
        city_gdf.to_csv(os.path.join(path_city_dir,city_name+'_buildings.csv'),index=False)

        # buildings
        city_boundary_gdf.to_csv(os.path.join(path_city_dir,city_name+'_boundaries.csv'),index=False)



print('Unmatched buildings in {}: {}'.format(region_name, len(region)))

city_counts.to_csv(os.path.join(path_output_dir,region_name+'count_buildings.csv'),
                  index=False)
