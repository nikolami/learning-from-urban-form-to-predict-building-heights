import pandas as pd
import geopandas as gpd
import os,sys
from shapely import wkt

base_path = '/data/metab/osm/ML_paper'
path_root = os.path.join(base_path,'git-plos-paper')
path_output_dir = os.path.join(base_path,'Data','2-data_preprocessed','Italy','Friuli_Veneto_Giulia')

sys.path.append(path_root)
from utils.preproc_utils import city_boundary_plus_buffers_gdf_wkt

crs = 7794
GDAM_level = 'NAME_3'

# import gdam
GDAM_file = gpd.read_file(os.path.join(path_to_data_1,'Data','1-data_primary','GADM','Italy','gadm36_ITA_3.shp'))
GDAM_file = GDAM_file.to_crs(crs) 


city_names = ['Capriva Del Friuli', 'Cormons', 'Doberdò del Lago', 'Dolegna Del Collio', "Farra d' Isonzo", 'Fogliano Redipuglia', 
'Gorizia', "Gradisca d' Isonzo", 'Grado', 'Mariano Del Friuli', 'Medea', 'Monfalcone', 'Moraro', 'Mossa', "Romans d' Isonzo", 
'Ronchi Dei Legionari', 'Sagrado', "San Canzian d' Isonzo", 'San Floriano Del Collio', 'San Lorenzo Isontino', "San Pier d' Isonzo", 
"Savogna d' Isonzo", 'Staranzano', 'Turriaco', 'Villesse', 'Andreis', 'Arba', 'Arzene', 'Aviano', 'Azzano Decimo', 'Barcis', 'Brugnera', 
'Budoia', 'Caneva', 'Casarsa Della Delizia', 'Castelnovo Del Friuli', 'Cavasso Nuovo', 'Chions', 'Cimolais', 'Claut', 'Clauzetto', 
'Cordenons', 'Cordovado', 'Erto E Casso', 'Fanna', 'Fiume Veneto', 'Fontanafredda', 'Frisanco', 'Maniago', 'Meduno', 'Montereale Valcellina', 
'Morsano Al Tagliamento', 'Pasiano Di Pordenone', 'Pinzano Al Tagliamento', 'Polcenigo', 'Porcia', 'Moggio Udinese', 'Moimacco', 'Montenars', 
'Pordenone', 'Sedegliano', 'Socchieve', 'Stregna', 'Sutrio', 'Taipana', 'Mortegliano', 'Moruzzo', 'Basiliano', 'Bertiolo', 'Muzzana Del Turgnano', 
'Bicinicco', 'Bordano', 'Talmassons', 'Tapogliano', 'Prata Di Pordenone', 'Nimis', 'Pravisdomini', 'Osoppo', 'Ovaro', 'Roveredo In Piano', 'Buja', 
'Pagnacco', 'Buttrio', 'Palazzolo Dello Stella', 'Camino Al Tagliamento', 'Palmanova', 'Paluzza', 'Tarcento', 'Campoformido', 'Campolongo Al Torre', 
'Carlino', 'Pasian Di Prato', 'Cassacco', 'Tarvisio', 'Paularo', 'Castions Di Strada', 'Cavazzo Carnico', 'Cercivento', 'Pavia Di Udine', 'Pocenia', 
'Sacile', 'Cervignano Del Friuli', 'Tavagnacco', 'Chiopris-Viscone', 'Pontebba', 'Chiusaforte', 'Teor', 'Porpetto', "Terzo d' Aquileia", 
'San Giorgio Della Richinvelda', 'San Martino Al Tagliamento', 'Povoletto', 'Tolmezzo', 'San Quirino', 'Cividale Del Friuli', 'Torreano', 
'Torviscosa', 'Trasaghis', 'Treppo Carnico', 'Pozzuolo Del Friuli', 'Treppo Grande', 'Pradamano', 'Prato Carnico', 'Precenicco', 'Tricesimo', 
'Premariacco', 'Preone', 'Prepotto', 'Trivignano Udinese', 'Pulfero', 'Ragogna', 'Ravascletto', 'Raveo', 'San Vito Al Tagliamento', 'Codroipo', 
'Sequals', 'Reana del Rojale', 'Colloredo Di Monte Albano', 'Comeglians', 'Corno Di Rosazzo', 'Coseano', 'Remanzacco', 'Dignano', 'Dogna', 'Drenchia', 
'Enemonzo', 'Resia', 'Resiutta', 'Sesto Al Reghena', 'Rigolato', "Rive d' Arcano", 'Faedis', 'Rivignano', 'Ronchis', 'Ruda', 'Fagagna', 'Fiumicello', 
'Flaibano', 'San Daniele Del Friuli', 'Forgaria Nel Friuli', 'Forni Avoltri', 'Forni Di Sopra', 'Forni Di Sotto', 'Spilimbergo', 'Tramonti Di Sopra',
'Tramonti Di Sotto', 'Travesio', 'Vajont', 'Valvasone', "Vito d' Asio", 'San Giorgio Di Nogaro', 'Vivaro', 'San Giovanni Al Natisone', 'San Leonardo', 
'San Pietro Al Natisone', 'San Vito Al Torre', 'San Vito Di Fagagna', 'Zoppola', 'Santa Maria La Longa', 'Sauris', 'Gemona Del Friuli', 'Gonars', 'Grimacco',
'Duino-Aurisina', 'Monrupino', 'Muggia', 'Latisana', 'Lauco', 'San Dorligo Della Valle', 'Lestizza', 'Sgonico', 'Lignano Sabbiadoro', 'Ligosullo', 
'Lusevera', 'Magnano In Riviera', 'Majano', 'Malborghetto Valbruna', 'Manzano', 'Marano Lagunare', 'Martignacco', 'Udine', 'Varmo', 'Venzone', 'Verzegnis', 
'Villa Santina', 'Villa Vicentina', 'Visco', 'Zuglio', 'Aiello Del Friuli', 'Amaro', 'Ampezzo', 'Aquileia', 'Arta Terme', 'Artegna', 'Attimis',
'Bagnaria Arsa', 'Mereto Di Tomba', 'Savogna', 'Trieste']


# for city in list:

for city_name in city_names:
    
        print(city_name)
        
        # create folder for the city
        path_output_city_dir = os.path.join(path_output_dir,city_name)
        os.makedirs(path_output_city_dir, exist_ok=True)
        
        # retrieve boundary 
        city_boundary_gdf = city_boundary_plus_buffers_gdf_wkt(city_name, GDAM_file, GDAM_level, crs)

        # write boundaries on disk
        city_boundary_gdf = pd.DataFrame(city_boundary_gdf).reset_index(drop=True)

        city_boundary_gdf.to_csv(os.path.join(path_output_city_dir,city_name)+'_boundaries.csv',
                  index=False)
