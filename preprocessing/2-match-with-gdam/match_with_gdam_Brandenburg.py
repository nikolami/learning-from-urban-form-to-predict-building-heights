import pandas as pd
import geopandas as gpd
from shapely import wkt
import os,sys

base_path = '/data/metab/osm/ML_paper'
path_root = os.path.join(base_path,'git-plos-paper')
path_output_dir = os.path.join(base_path,'Data','2-data_preprocessed','Germany','Brandenburg')

sys.path.append(path_root)
from utils.preproc_utils import retrieve_city_boundary_gdf, city_boundary_plus_buffers_gdf_wkt
from utils.helpers import import_csv_w_wkt_to_gdf,import_csv_w_wkt_to_gdf_bb

crs = 25833
region_name = 'Brandenburg'
GDAM_level = 'NAME_4'


# import gdam
GDAM_file = gpd.read_file(os.path.join(base_path,'Data','1-data_primary','GADM','Germany','gadm36_DEU_4.shp'))
GDAM_file = GDAM_file.to_crs(crs) 

# import buildings
region = import_csv_w_wkt_to_gdf_bb(os.path.join(path_to_data_2,'Data','1-data_primary','3d_models',
                                                    'Germany','Brandenburg','Brandenburg.csv'),
            crs)

## Remove buildings within buffer to other German regions 

# import buffer 
path_no_pol = os.path.join(path_to_data_2,'1-Preprocessing','Germany','Brandenburg','2-match_w_gdam','within_500_no_pol.csv')
within_500_no_pol = import_csv_w_wkt_to_gdf(path_no_pol,crs)

# retrieve buildings to be removed
building_500_in = gpd.sjoin(region, within_500_no_pol, how='inner', op='within')
region = region.drop(building_500_in.index)
print('there are {} buildings within 500m of the limits of Brandenburg (excluding the Polish Border)'.format(len(building_500_in)))

## Matching

# initialize 
city_counts = pd.DataFrame(columns = ['city', 'count'])
city_names = list(GDAM_file[GDAM_file.NAME_1==region_name][GDAM_level])
print(city_names)


for city_name in city_names:

        print(city_name)

        city_name_gdam = city_name

        if '/' in city_name:  
            city_name = city_name.replace('/','-')
        

        ## do Buildings
        
        # get boundary
        city_boundary_gdf = retrieve_city_boundary_gdf(city_name, GDAM_file, GDAM_level, crs)

        # get buildings in boundary
        city_gdf = gpd.sjoin(region,city_boundary_gdf, how='inner',op='intersects')
        city_gdf = city_gdf.drop(columns=['index_right'])
        print(len(city_gdf))

        # remove buildings from region
        region = region.drop(city_gdf.index)
        
        # store counts
        city_counts = city_counts.append(pd.DataFrame({'region':departement_name,
                                                        'city':city_name,
                                                        'count':len(city_gdf)},index = [0]))
        ## do Boundaries w/ buffers
        
        city_boundary_gdf = city_boundary_plus_buffers_gdf_wkt(city_name, GDAM_file, GDAM_level, crs)

        ## Save

        if len(city_gdf) > 0:

            path_city_dir = os.path.join(path_output_dir,city_name)

            os.makedirs(path_city_dir, exist_ok=True)

           # boundaries
            city_gdf['geometry'] = city_gdf['geometry'].apply(lambda x: x.wkt)
            city_gdf = pd.DataFrame(city_gdf).reset_index(drop=True)
            city_gdf.to_csv(os.path.join(path_city_dir,city_name+'_buildings.csv'),index=False)

            # buildings
            city_boundary_gdf.to_csv(os.path.join(path_city_dir,city_name+'_boundaries.csv'),index=False)
                   


print('Unmatched buildings: {}'.format(len(region)))

city_counts.to_csv(os.path.join(path_output_dir,'count_buildings.csv'),
                  index=False)
