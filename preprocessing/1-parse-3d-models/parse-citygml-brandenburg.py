import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, sys

# set the path to the root directory of the git
path_root = r'C:/Users/miln/tubCloud/Work-in-progress/main-project/new_git'

# enable python to find the modules
sys.path.append(path_root)

# import own functions
from utils.preproc_utils import parse_citygml




### =========================================================================
### ======================== SET PARAMETERS =================================
### =========================================================================


### SET THE PARAMETERS FOR ONE OPTION BELOW, AND THE OTHERS TO `None`

### if single_file == True --------------------------------------------------

# # provide the file name
file_name = None

# # provide the path to the file
path_file = None

### if single_file == False -------------------------------------------------

# provide path to directory where all files are stored
path_directory = r'E:/Data/data-primary/Germany/Brandenburg'

# optionnaly, choose a range of files by changing this
# by default it runs until the end
run_from_file = 0
run_until_file = len(os.listdir(path_directory))

### COMMON PARAMETERS

# path output
path_output = r'E:/Data/data-preprocessed/Germany/Brandenburg/Brandenburg.csv'

# choose the coordinate reference system
crs = 'epsg:25833'

# provide the area info
# Note: the filename can be the name of the city or district, put it in the
#       relevant position without the file ending like this: file_name[:,-3] 
# Note: if the district or city is missing, put np.nan instead
area_info = ['Germany', 'Brandenburg', np.nan, np.nan, file_name]

# choose if single or multiple files
single_file = False

## compute the height using the bounding box
bbox = False



### =========================================================================
### =========================================================================
### =========================================================================




### PARSING FILE


# if single file
if single_file:

	# parse
	df = parse_citygml(path_file,area_info,crs,bbox)


# if several files 
else:

	df = pd.DataFrame()

	# iterate over files in the directory
	for i, file_name in enumerate(os.listdir(path_directory)[run_from_file:run_until_file]):

		print(file_name)

		# assemble path
		path_file = os.path.join(path_directory,file_name)	

		# parse
		df_part = parse_citygml(path_file,area_info,crs,bbox)

		# concatenate new 
		df = pd.concat([df, df_part])


### DIAGNOSTICS 


# check missing values
print('Missing footprint polygons: {}'.format(df['geometry'].isnull().sum()))
print('Missing measured height: {}'.format(df['height_measured'].isnull().sum()))

# check for potential mismatches between height_measured and height_bbox
if bbox:
		print(df['height_bbox'].isnull().sum())

		diff = df['height_measured'] - df['height_bbox']
				print(np.mean(diff))
				print(sum(diff > 2))
				print(sum(diff < -2))

# plot heights histogram
plt.hist(df['height_measured'])


### SAVING FILE


# drop buildings with missing polygon
df.dropna(subset = ['geometry'], inplace=True)

# save the file
df.to_csv(path_output, index=False)

