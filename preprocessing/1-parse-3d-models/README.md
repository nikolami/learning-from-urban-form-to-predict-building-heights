# Parsing 3D models

## Downloading 3D models

The first step of the workflow is to download 3D models.

### The Netherlands

The data can be downloaded on the following website as one file. It is also necessary to download a file that maps the administrative codes to city names. As this file is being updated regularly and changes, the file we used is available in the directory as `oversicht.csv`.

### Berlin

The data can be downloaded on the following [website](https://www.businesslocationcenter.de/en/economic-atlas/download-portal/) in 12 parts, which correspond to the Berlin districts.

### Brandenburg

The data can be downloaded on the following [website](https://geobasis-bb.de/lgb/de/geodaten/3d-produkte/3d-gebaeudemodelle/) in many parts, which correspond to small bounding boxes. One can download all the files at once by running the following bash code:

```
# make sure that you create a directory (where you want to download the data in) in advance and that you have enough memory for the downolad (~ 50 GB).
# navigate to the directory and proceed with the following steps:

# 1. download all .zip files (source: https://stackoverflow.com/questions/13533217/how-to-download-all-links-to-zip-files-on-a-given-web-page-using-wget-curl)

wget -r -np -l 1 -A zip https://data.geobasis-bb.de/geobasis/daten/3d_gebaeude/lod2_gml/

#-r,  --recursive          specify recursive download.
#-np, --no-parent          don't ascend to the parent directory.
#-l,  --level=NUMBER       maximum recursion depth (inf or 0 for infinite).
#-A,  --accept=LIST        comma-separated list of accepted extensions.


# 2. unzip all files in folder (source: https://stackoverflow.com/questions/2374772/unzip-all-files-in-a-directory/29248777)
#    if not yet installed, you need to install the package unzip via: sudo apt install unzip

unzip \*.zip

# 3. remove all files except .gml files (source https://www.tecmint.com/delete-all-files-in-directory-except-one-few-file-extensions/), while displaying what is being done (-v).

rm -v !(*.gml)
```

### Friuli-Venezia Giulia

The data comes from OpenStreetMap and is downloaded later (step 3).

### France

The data can be accessed individually on the websites of the cities of [Bordeaux](https://opendata.bordeaux-metropole.fr/explore/dataset/bati3d/information/), [Brest](https://geo.pays-de-brest.fr/zapp/Pages/Donnees3D.aspx), [Lyon](https://data.grandlyon.com/jeux-de-donnees/maquettes-3d-texturees-a-commune-arrondissement-2009-2012-2015-metropole-lyon/info), [Montpellier](http://data.montpellier3m.fr/dataset/photomodele-3d-urbain-de-la-ville-de-montpellier) and [Strasbourg](https://data.strasbourg.eu/explore/dataset/odata3d_maquette/custom/).

For this project, we used data preprocessed by NamR. The preprocessed data is available [here](https://drive.google.com/file/d/1M7tojg9WMDUl5vLy0vpmOyBwJzY92_vv/view?usp=sharing).


## Parsing the data

For this project, we need from the 3D models for each building the footprint polygon, a height value and an id. At the end of this step, we save buildings as .csv files, with geometries written in WKT.

Berlin and Brandenburg come as CityGML and are parsed in a similar way, to extract the relevant information from more complex geospatial objects.

The Netherlands come as a GeoPackage, we convert the format and parse the information in the file.

The data for France is already is the desired format.

