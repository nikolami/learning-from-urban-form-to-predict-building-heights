#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=ols-%A_%a
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=80000
#SBATCH --output=ols_out-%A_%a.txt
#SBATCH --error=ols_err-%A_%a.txt
#SBATCH --array=0-3

pwd; hostname; date

module load anaconda

source activate urban_form

python -u cv-ols.py -i $SLURM_ARRAY_TASK_ID
