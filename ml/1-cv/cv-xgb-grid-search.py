import pandas as pd
import os
import sys
import time 
import matplotlib.pyplot as plt 
import argparse
import xgboost


# paths 
path_root = '/data/metab/learning-from-urban-form-to-predict-building-heights/git-plos-paper'
sys.path.append(path_root)
import utils.ml_utils as mlu

path_to_data = '/data/metab/learning-from-urban-form-to-predict-building-heights/data'
path_results = os.path.join(path_to_data,'3-ML','v_2','csv-xgb')
path_dfs = os.path.join(path_to_data,'3-ML','Data_v_2')

############### Experiment set-up ##############################

# Experiment

# parameter file
path_params = os.path.join(path_root,'ml','1-cv','hyperparameters.csv')
params = pd.read_csv(path_params)

# argument parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', help="SLURM_ARRAY_TASK_ID", type=int)
args = parser.parse_args()

## Settings
CV = params.iloc[args.i].CV
valid_fold = params.iloc[args.i].valid_fold
df_train = params.iloc[args.i].df_train


print(int(args.i))
print(CV)
print(df_train)
print(valid_fold)


## Parameters
loss = params.iloc[args.i].loss
learning_rate = params.iloc[args.i].learning_rate
max_depth = params.iloc[args.i].max_depth
subsample = params.iloc[args.i].subsample
colsample_bytree = params.iloc[args.i].colsample_bytree
gamma = params.iloc[args.i].gamma
min_child_weight = params.iloc[args.i].min_child_weight

## Model

# model set-up
model = xgboost.XGBRegressor(loss=loss,
    learning_rate=learning_rate,
    max_depth=max_depth,
    subsample=subsample,
    colsample_bytree=colsample_bytree,
    gamma=gamma,
    min_child_weight=min_child_weight)



############### Import data ######################################

# import
X_train = pd.read_csv(os.path.join(path_dfs,df_train+'.csv'))
X_valid = pd.read_csv(os.path.join(path_dfs,valid_fold+'.csv'))

# parse
X_train,X_valid,y_train,y_valid,df_valid,city_counts = mlu.parse_input(X_train,X_valid,count_cities=True)


############### Learn ########################################

print('---')
print('Learning...')

metrics_df = pd.DataFrame()

metrics_df.loc[0,'experiment'] = int(args.i)
metrics_df['CV'] = CV
metrics_df['valid_fold'] = valid_fold
metrics_df['loss'] = loss
metrics_df['learning_rate'] = learning_rate
metrics_df['max_depth'] = max_depth
metrics_df['subsample'] = subsample
metrics_df['colsample_bytree'] = colsample_bytree
metrics_df['gamma'] = gamma
metrics_df['min_child_weight'] = min_child_weight


# train
start = time.time()

model.fit(X_train,y_train, eval_set=[(X_train,y_train),(X_valid,y_valid)], 
    eval_metric = ['mae'],
    verbose=True, 
    early_stopping_rounds=50)

end = time.time()
last = divmod(end - start, 60)
print('Fit in {} minutes {} seconds'.format(last[0],last[1])) 

# get best number of trees
metrics_df['ntree_limit'] = model.get_booster().best_ntree_limit

# predict
y_predict = model.predict(X_valid, ntree_limit=model.get_booster().best_ntree_limit)

# write the results
df_valid['y_predict'] = y_predict
df_valid['error'] = df_valid['y_predict'] - df_valid['y_valid']
df_valid['error_abs'] = abs(df_valid['error'])


############### Metrics #######################################

df_valid,city_results,height_groups_1,height_groups_2 = mlu.compute_metrics(df_valid,X_valid,model,city_counts)

metrics_df = mlu.get_ft_importance_xgb(model,metrics_df)

metrics_df.to_csv(os.path.join(path_results,'metrics',str(args.i)+'.csv'),
    index=False)
