#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=grid_cv-%A_%a
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=14
#SBATCH --output=grid_cv_out-%A_%a.txt
#SBATCH --error=grid_cv_err-%A_%a.txt
#SBATCH --array=0-1999

pwd; hostname; date

module load anaconda

source activate urban_form

python -u cv-xgb-grid-search.py -i $SLURM_ARRAY_TASK_ID
