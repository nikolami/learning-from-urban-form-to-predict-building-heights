import pandas as pd
import os
import sys
import time 
import matplotlib.pyplot as plt 
import argparse
from sklearn.linear_model import LinearRegression


# paths 
path_root = '/data/metab/learning-from-urban-form-to-predict-building-heights/git-plos-paper'
sys.path.append(path_root)
import utils.ml_utils as mlu

path_to_data = '/data/metab/learning-from-urban-form-to-predict-building-heights/data'
path_results = os.path.join(path_to_data,'3-ML','v_2','cv-ols')
path_dfs = os.path.join(path_to_data,'3-ML','Data_v_2')


list_df_train = ['it_nl1_nl2','fr_nl1_nl2','fr_it_nl2','fr_it_nl1']
list_valid_fold = ['fr','it','nl1','nl2']


############### Experiment set-up ##############################

# Experiment
# parameter file
path_params = os.path.join(path_to_data_2,'3-ML','v_2','RF','hyperparameters_rf.csv')
params = pd.read_csv(path_params)

# argument parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', help="SLURM_ARRAY_TASK_ID", type=int)
args = parser.parse_args()


## Settings
CV = params.iloc[args.i].CV
valid_fold = params.iloc[args.i].valid_fold
df_train = params.iloc[args.i].df_train

print(int(args.i))
print(CV)
print(df_train)
print(valid_fold)

############### Experiment set-up ##############################

## Model

# model set-up
model = LinearRegression()

############### Import data ######################################

# import
X_train = pd.read_csv(os.path.join(path_dfs,df_train+'.csv'))
X_valid = pd.read_csv(os.path.join(path_dfs,valid_fold+'.csv'))

# parse
X_train,X_valid,y_train,y_valid,df_valid,city_counts = mlu.parse_input(X_train,X_valid,count_cities=True)

############### Learn ########################################

print('---')
print('Learning...')


# train
start = time.time()

model.fit(X_train,y_train)

end = time.time()
last = divmod(end - start, 60)
print('Fit in {} minutes {} seconds'.format(last[0],last[1])) 


# predict
y_predict = model.predict(X_valid)


# write the results
df_valid['y_predict'] = y_predict
df_valid['error'] = df_valid['y_predict'] - df_valid['y_valid']
df_valid['error_abs'] = abs(df_valid['error'])

df_valid.to_csv(os.path.join(path_results,str(args.i)+'full_results.csv'),index=False)


############### Metrics #######################################

df_valid,city_results,height_groups_1,height_groups_2 = mlu.compute_metrics(df_valid,X_valid,model,city_counts)

############### Saving results ####################################

metrics_df.to_csv(os.path.join(path_results,'metrics',str(args.i)+'.csv'),
    index=False)










