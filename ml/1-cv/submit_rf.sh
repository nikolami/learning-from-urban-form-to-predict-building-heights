#!/bin/bash

#SBATCH --qos=short
#SBATCH --constraint=broadwell
#SBATCH --job-name=cv_rf-%A_%a
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=cv_rf_out-%A_%a.txt
#SBATCH --error=cv_rf_err-%A_%a.txt
#SBATCH --array=0-3

pwd; hostname; date

module load anaconda

source activate urban_form

python -u cv_rf.py -i $SLURM_ARRAY_TASK_ID
