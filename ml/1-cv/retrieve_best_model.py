import pandas as pd
import os
import glob

path_to_data = '/data/metab/learning-from-urban-form-to-predict-building-heights/data'

# glob all files in folder
list_path_files = glob.glob(os.path.join(path_to_data,'3-ML','v_2','grid_CV_results','xgb','metrics','*'))

## Assemble DF
# empty dataframe
metrics_df = pd.DataFrame()

# for file in folder
for path in list_path_files:

	metrics_add = pd.read_csv(path)
	metrics_df = metrics_df.append(metrics_add)

metrics_df.to_csv(os.path.join(path_to_data,'3-ML','v_2','Model_Selection','metrics_grid_search.csv'),index=False)

## group by CV
metrics_df_by_cv = metrics_df.groupby('CV')['MAE'].mean()

metrics_df_by_cv = pd.DataFrame(metrics_df_by_cv.sort_values(ascending=True))
metrics_df_by_cv['CV'] = metrics_df_by_cv.index

print(metrics_df_by_cv)


metrics_df_by_cv.to_csv(os.path.join(path_to_data,'3-ML','v_2','Model_Selection','metrics_grid_search_by_cv.csv'),index=False)

