import pandas as pd
import os
import sys
import time 
import matplotlib.pyplot as plt 
import argparse
import xgboost


# paths 
path_root = '/data/metab/learning-from-urban-form-to-predict-building-heights/git-plos-paper'
sys.path.append(path_root)
import utils.ml_utils as mlu

path_to_data = '/data/metab/learning-from-urban-form-to-predict-building-heights/data'
path_results = os.path.join(path_to_data,'3-ML','v_2','test_ber')
path_dfs = os.path.join(path_to_data,'3-ML','Data_v_2')

############### Experiment set-up ##############################

exp_name = 'exp-1-bra'
df_train = 'fr_it_NL'
valid_fold = 'bra_90'

## Experiment

# parameter file
path_params = os.path.join(path_root,'ml','1-cv','hyperparameters.csv')
params = pd.read_csv(path_params)

# argument parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', help="SLURM_ARRAY_TASK_ID", type=int)
args = parser.parse_args()


## Settings
CV = params.iloc[args.i].CV

print(int(args.i))
print(CV)
print(df_train)
print(valid_fold)


## Parameters
loss = params.iloc[args.i].loss
learning_rate = params.iloc[args.i].learning_rate
max_depth = params.iloc[args.i].max_depth
subsample = params.iloc[args.i].subsample
colsample_bytree = params.iloc[args.i].colsample_bytree
gamma = params.iloc[args.i].gamma
min_child_weight = params.iloc[args.i].min_child_weight


## Model

# model set-up
model = xgboost.XGBRegressor(loss=loss,
    n_estimators=250,
    learning_rate=learning_rate,
    max_depth=max_depth,
    subsample=subsample,
    colsample_bytree=colsample_bytree,
    gamma=gamma,
    min_child_weight=min_child_weight,
    nthreads=-1)


############### Import data ######################################

# import
X_train = pd.read_csv(os.path.join(path_dfs,df_train+'.csv'))
X_valid = pd.read_csv(os.path.join(path_dfs,valid_fold+'.csv'))

# parse
X_train,X_valid,y_train,y_valid,df_valid,city_counts = mlu.parse_input(X_train,X_valid,count_cities=True)

############### Learn ########################################

print('---')
print('Learning...')

metrics_df = pd.DataFrame()

metrics_df.loc[0,'experiment'] = int(args.i)
metrics_df['CV'] = CV
metrics_df['valid_fold'] = valid_fold


# train
start = time.time()

model.fit(X_train,y_train)

end = time.time()
last = divmod(end - start, 60)
print('Fit in {} minutes {} seconds'.format(last[0],last[1])) 


# predict
y_predict = model.predict(X_valid)

# write the results
df_valid['y_predict'] = y_predict
df_valid['error'] = df_valid['y_predict'] - df_valid['y_valid']
df_valid['error_abs'] = abs(df_valid['error'])


############### Metrics #######################################

df_valid,city_results,height_groups_1,height_groups_2 = mlu.compute_metrics(df_valid,X_valid,model,city_counts)

metrics_df = mlu.get_ft_importance_xgb(model,metrics_df)


############### Plots #######################################

# distribution target vs predicted
name_plot= '{}-distr'.format(exp_name)
fig, ax = mlu.plot_two_height_distributions(df_valid['y_predict'],df_valid['y_valid'],
                                            'y_predict','y_valid',
                                            name_plot)
plt.savefig(os.path.join(path_results,name_plot+'.png'),dpi=300)

## joint plot
name_plot= '{}-joint'.format(exp_name)
fig = mlu.plot_joint_height_distr(df_valid,name_plot)
plt.savefig(os.path.join(path_results,name_plot+'.png'),dpi=300)

## plot cumulative errors
name_plot= '{}-cumulative_error'.format(exp_name)
fig, ax = mlu.plot_cum_errors(df_valid,name_plot)
plt.savefig(os.path.join(path_results,name_plot+'.png'),
    dpi=300)

## plot errors
name_plot= '{}-error'.format(exp_name)
fig, ax = mlu.plot_error_distribution(df_valid,name_plot)
plt.savefig(os.path.join(path_results,name_plot+'.png'),
    dpi=300)

## violion plot errors
name_plot= '{}-violin'.format(exp_name)
fig, ax = mlu.violion_plot_errors(df_valid,name_plot)
plt.savefig(os.path.join(path_results,name_plot+'.png'),
    dpi=300)
plt.savefig(os.path.join(path_results,name_plot+'.svg'),
    dpi=300)

# save
metrics_df.to_csv(os.path.join(path_results,exp_name+'-error-metrics.csv'),
    index=False)

df_valid.to_csv(os.path.join(path_results,exp_name+'-full_results.csv'),index=False)

city_results.to_csv(os.path.join(path_results,exp_name+'-city_results.csv'),
    index=False)

