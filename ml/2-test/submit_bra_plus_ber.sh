#!/bin/bash

#SBATCH --qos=medium
#SBATCH --job-name=test_BB-%A_%a
#SBATCH --constraint=broadwell
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=test_bra_out-%A_%a.txt
#SBATCH --error=test_bra_err-%A_%a.txt

pwd; hostname; date

module load anaconda

source activate urban_form

python -u exp-3-bra.py -i 253 
