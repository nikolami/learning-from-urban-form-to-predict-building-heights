#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=sample_ber-%A_%a
#SBATCH --constraint=broadwell
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=test_ber_out-%A_%a.txt
#SBATCH --error=test_ber_err-%A_%a.txt
#SBATCH --array=0-4

pwd; hostname; date

module load anaconda

source activate urban_form

python -u exp-2-ber.py -i 253 -s $SLURM_ARRAY_TASK_ID
