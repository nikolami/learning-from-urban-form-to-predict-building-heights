#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=test_ber-%A_%a
#SBATCH --partition=standard
#SBATCH --constraint=broadwell
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=test_ber_out-%A_%a.txt
#SBATCH --error=test_ber_err-%A_%a.txt

pwd; hostname; date

module load anaconda

source activate urban_form

python -u exp-1-ber.py -i 253 
