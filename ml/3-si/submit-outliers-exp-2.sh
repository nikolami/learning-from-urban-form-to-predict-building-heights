#!/bin/bash

#SBATCH --qos=medium
#SBATCH --job-name=out_bra_sample-%A_%a
#SBATCH --constraint=broadwell
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=test_bra_out-%A_%a.txt
#SBATCH --error=test_bra_err-%A_%a.txt
#SBATCH --array=0-4

pwd; hostname; date

module load anaconda

source activate urban_form

python -u s12-outliers-exp-2-bra.py.py -i 253 -s $SLURM_ARRAY_TASK_ID -t 2
