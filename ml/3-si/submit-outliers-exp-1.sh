#!/bin/bash

#SBATCH --qos=medium
#SBATCH --job-name=out_bra-%A_%a
#SBATCH --constraint=broadwell
#SBATCH --partition=standard
#SBATCH --account=metab
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --output=bra_out-%A_%a.txt
#SBATCH --error=bra_err-%A_%a.txt
#SBATCH --array=0-3

pwd; hostname; date

module load anaconda

source activate urban_form

python -u s12-outliers-exp-1-bra.py -i 253 -t $SLURM_ARRAY_TASK_ID
