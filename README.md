# Learning from urban form to predict building heights

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This is a companion repository to the study _Learning from urban form to predict building heights_ published in PLOS ONE. https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0242010

Disclaimer: this git is currently incomplete.

## What's on this repo

In this repo, you will find all the code we used in the study. You will not find the data because they were too large to be hosted here. However, you will find links to download the raw data, which enables to reproduce all the steps from scratch. 

Also note that large parts of the processing have been implemented on a cluster, with a large RAM and parallelization over multiple CPUs. Such parts can be easily adapted to run on smaller chuncks of data on a laptop.

We provide a tutorial on how to realize the different steps of the pipeline, and sample data to reproduce it.

We also provide the list of libraries we used, with their version, in the file `requirements.txt`. To be sure that the code will not break due to version issues, you can create a virtual environment with Python 3.8.6 and these libraries versions. To do so, if you are using Anaconda you can run in your command prompt:

```
conda create --name <env> --file requirements.txt 
```


## Authors 

The code has been authored by Nikola Milojevic-Dupont, Francois Andrieux and Nicolai Hans.

If you are reusing the content in another research paper, here is how you can cite our paper:


> Milojevic-Dupont N, Hans N, Kaack LH, Zumwald M, Andrieux F, de Barros Soares D, et al. (2020) Learning from urban form to predict building heights. PLoS ONE 15(12): e0242010. https://doi.org/10.1371/journal.pone.0242010


## Contact

For any question about the code in this repo, contact Nikola Milojevic-Dupont (milojevic@mcc-berlin.net). 



