import pandas as pd
import geopandas as gpd
import numpy as np
from shapely import wkt
from shapely.geometry import MultiPolygon


def import_csv_w_wkt_to_gdf(path,crs):
    '''
    Import a csv file with WKT geometry column into a GeoDataFrame

    Last modified: 12/09/2020. By: Nikola

    '''

    df = pd.read_csv(path)
    df.geometry = df.geometry.apply(wkt.loads)
    gdf = gpd.GeoDataFrame(df, 
                                geometry=df.geometry,
                                crs=crs)
    return(gdf)


def import_csv_w_wkt_to_gdf_namr(path,crs):
    df = pd.read_csv(path)
    df.geometry = df.the_geom.apply(wkt.loads)
    df.rename(columns={'the_geom':'geometry'}, inplace=True)
    gdf = gpd.GeoDataFrame(df, 
                            geometry=df.geometry,
                            crs=crs)
    
    return(gdf)


def import_csv_w_wkt_to_gdf_bb(path,crs):

    df = pd.read_csv(path)
    df.rename(columns={'polygon':'geometry'}, inplace=True)
    df = df.dropna(subset=['geometry'])
    df['geometry'] = df['geometry'].apply(wkt.loads)
    gdf = gpd.GeoDataFrame(df, 
                            geometry=df.geometry,
                            crs=crs)
    return(gdf)


def multipoly_to_largest_poly(mutlipoly):
    '''
    Turn a multipolygon into the largest largest available polygon.

    Last modified: 26/01/2021. By: Nikola

    '''
    largest_poly_index = np.argmax([poly.area for poly in mutlipoly])
    largest_poly = mutlipoly[largest_poly_index]

    return(largest_poly)

def GDF_multipoly_to_largest_poly(gdf):
    '''
    Turn a multipolygon into the largest largest available polygon.

    Last modified: 27/01/2021. By: Nikola

    '''
    index_multi = [ind for ind, x in enumerate(gdf.geometry) if type(x) == MultiPolygon]

    for index in index_multi:

        gdf.loc[index,'geometry'] = multipoly_to_largest_poly(gdf.loc[index,'geometry'])

    # geom_list = [None] * len(gdf)
    # for index,row in gdf.iterrows():
    #     if type(row.geometry) == MultiPolygon:
    #         geom_list[index] = multipoly_to_largest_poly(row.geometry)
    #     else:
    #         geom_list[index] = row.geometry
    
    return(gdf)


def is_namr_data_available(GDAM_file,region_hull,city_name):

    city_boundary = GDAM_file[GDAM_file['NAME_4']==city_name].geometry.iloc[0]

    if city_boundary.intersects(region_hull) == False:
        print('{} -> no'.format(city_name))
        is_data_available = False

    else:
        print('{} -> yes'.format(city_name))
        is_data_available = True
        return(is_data_available)