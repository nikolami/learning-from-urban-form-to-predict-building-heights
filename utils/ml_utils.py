import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from collections import Counter
from sklearn import metrics
import seaborn as sns
import matplotlib.pyplot as plt 



features_groups = {

'buildings':    ['FootprintArea', 'Perimeter', 'Phi', 'LongestAxisLength', 'Elongation', 
'Convexity', 'Orientation', 'Corners', 'CountTouches', 'SharedWallLength'], 

'blocks':   ['BlockLength', 'AvBlockFootprintArea', 'StdBlockFootprintArea', 'BlockTotalFootprintArea', 
'BlockPerimeter', 'BlockLongestAxisLength', 'BlockElongation', 'BlockConvexity', 'BlockOrientation', 'BlockCorners'],

'dist_based_bu_50': ['std_block_orientation_within_buffer_50', 'av_block_orientation_within_buffer_50', 'av_block_av_footprint_area_within_buffer_50', 
'std_block_footprint_area_within_buffer_50', 'av_block_footprint_area_within_buffer_50', 'std_block_length_within_buffer_50', 
'av_block_length_within_buffer_50', 'blocks_within_buffer_50', 'std_orientation_within_buffer_50', 'av_orientation_within_buffer_50', 
'std_convexity_within_buffer_50', 'av_convexity_within_buffer_50', 'std_elongation_within_buffer_50', 'av_elongation_within_buffer_50', 
'std_footprint_area_within_buffer_50', 'av_footprint_area_within_buffer_50', 'total_ft_area_within_buffer_50', 'buildings_within_buffer_50'], 

'dist_based_bu_200': ['std_block_orientation_within_buffer_200', 'av_block_orientation_within_buffer_200', 'av_block_av_footprint_area_within_buffer_200', 
'std_block_footprint_area_within_buffer_200', 'av_block_footprint_area_within_buffer_200', 'std_block_length_within_buffer_200', 
'av_block_length_within_buffer_200', 'blocks_within_buffer_200', 'std_orientation_within_buffer_200', 'av_orientation_within_buffer_200', 
'std_convexity_within_buffer_200', 'av_convexity_within_buffer_200', 'std_elongation_within_buffer_200', 'av_elongation_within_buffer_200', 
'std_footprint_area_within_buffer_200', 'av_footprint_area_within_buffer_200', 'total_ft_area_within_buffer_200', 'buildings_within_buffer_200'], 

'dist_based_bu_500': ['std_block_orientation_within_buffer_500', 'av_block_orientation_within_buffer_500', 'av_block_av_footprint_area_within_buffer_500', 
'std_block_footprint_area_within_buffer_500', 'av_block_footprint_area_within_buffer_500', 'std_block_length_within_buffer_500', 
'av_block_length_within_buffer_500', 'blocks_within_buffer_500', 'std_orientation_within_buffer_500', 'av_orientation_within_buffer_500', 
'std_convexity_within_buffer_500', 'av_convexity_within_buffer_500', 'std_elongation_within_buffer_500', 'av_elongation_within_buffer_500', 
'std_footprint_area_within_buffer_500', 'av_footprint_area_within_buffer_500', 'total_ft_area_within_buffer_500', 'buildings_within_buffer_500'], 

'str_int_closest':  ['street_closeness_500_closest_road', 'street_betweeness_global_closest_road', 'street_closeness_global_closest_road', 'street_openness_closest_road', 
'street_width_std_closest_road', 'street_width_av_closest_road', 'street_length_closest_road', 'distance_to_closest_road', 'distance_to_closest_intersection'], 

'str_int_50':   ['street_closeness_500_av_inter_buffer_50', 'street_closeness_500_max_inter_buffer_50', 'street_betweeness_global_av_inter_buffer_50', 
'street_betweeness_global_max_inter_buffer_50', 'street_width_std_inter_buffer_50', 'street_width_av_inter_buffer_50', 
'street_length_total_inter_buffer_50', 'street_length_std_within_buffer_50', 'street_length_av_within_buffer_50', 
'street_length_total_within_buffer_50', 'intersection_count_within_buffer_50'], 

'str_int_200':  ['street_closeness_500_av_inter_buffer_200', 'street_closeness_500_max_inter_buffer_200', 'street_betweeness_global_av_inter_buffer_200', 
'street_betweeness_global_max_inter_buffer_200', 'street_width_std_inter_buffer_200', 'street_width_av_inter_buffer_200', 
'street_length_total_inter_buffer_200', 'street_length_std_within_buffer_200', 'street_length_av_within_buffer_200', 
'street_length_total_within_buffer_200', 'intersection_count_within_buffer_200'], 

'str_int_500':  ['street_closeness_500_av_inter_buffer_500', 
'street_closeness_500_max_inter_buffer_500', 'street_betweeness_global_av_inter_buffer_500', 'street_betweeness_global_max_inter_buffer_500', 
'street_width_std_inter_buffer_500', 'street_width_av_inter_buffer_500', 'street_length_total_inter_buffer_500', 'street_length_std_within_buffer_500', 
'street_length_av_within_buffer_500', 'street_length_total_within_buffer_500', 'intersection_count_within_buffer_500'], 

'str_sbb':  ['street_based_block_corners', 'street_based_block_phi', 'street_based_block_area'], 

'str_sbb_50':   ['street_based_block_std_orientation_inter_buffer_50', 'street_based_block_std_phi_inter_buffer_50', 'street_based_block_av_phi_inter_buffer_50', 
'street_based_block_std_area_inter_buffer_50', 'street_based_block_av_area_inter_buffer_50', 'street_based_block_number_inter_buffer_50'], 

'str_sbb_200':  ['street_based_block_std_orientation_inter_buffer_200', 'street_based_block_std_phi_inter_buffer_200', 'street_based_block_av_phi_inter_buffer_200', 
'street_based_block_std_area_inter_buffer_200', 'street_based_block_av_area_inter_buffer_200', 'street_based_block_number_inter_buffer_200'], 

'str_sbb_500':  ['street_based_block_std_orientation_inter_buffer_500', 'street_based_block_std_phi_inter_buffer_500', 'street_based_block_av_phi_inter_buffer_500', 
'street_based_block_std_area_inter_buffer_500', 'street_based_block_av_area_inter_buffer_500', 'street_based_block_number_inter_buffer_500'], 

'city_level': ['area_city', 'phi_city', 'total_buildings_city', 'av_building_footprint_city', 'std_building_footprint_city', 
'num_detached_buildings', 'block_2_to_5', 'block_6_to_10', 'block_11_to_20', 'block_20+', 'total_intersection_city', 
'total_length_street_city', 'av_length_street_city', 'total_number_block_city', 'av_area_block_city', 'std_area_block_city']

}




def parse_input(X_train,X_valid,count_cities=False):
    '''Parse inputs for the ML model.
    '''

    X_train = shuffle(X_train,random_state=0)
    X_valid = shuffle(X_valid,random_state=0)

    # retrieve target vectors
    y_train = np.array(X_train[['height']])
    y_valid = np.array(X_valid[['height']])

    # keep ancilliary variables for analysis
    df_valid = X_valid[['height','id','centroid_WGS84','geometry']]
    df_valid.rename(columns = {'height':'y_valid'}, inplace=True)

    # retrieve feature matrices
    X_train = X_train.drop(columns=['height','id','centroid_WGS84','geometry','city'])
    X_valid = X_valid.drop(columns=['height','id','centroid_WGS84','geometry','city'])

    print('---')
    print('Shapes:')
    print('Training set:')
    print(X_train.shape)
    print('Validation set:')
    print(X_valid.shape)

    if count_cities:
        # retrieve city counts
        city_counts = Counter(X_valid.city)
        city_counts = pd.DataFrame.from_dict(city_counts, orient='index').reset_index()
        city_counts = city_counts.rename(columns={'index':'city', 0:'count'})

        return(X_train,X_valid,y_train,y_valid,df_valid,city_counts)

    else:

        return(X_train,X_valid,y_train,y_valid,df_valid) 





def city_metrics(df):
    '''
    Used in compute_metrics to return city-level metrics.

    Last modified: 12/21/2020. By: Nikola

    '''

    MAE = metrics.mean_absolute_error(df['y_valid'],df['y_predict'])
    R2 = metrics.r2_score(df['y_valid'],df['y_predict'])
    return pd.Series(dict(MAE=MAE,R2=R2))



def compute_metrics(df_valid,X_valid,model,city_counts=None):
    '''
    '''

    y_predict = model.predict(X_valid)

    # write the results
    df_valid['y_predict'] = y_predict
    df_valid['error'] = df_valid['y_predict'] - df_valid['y_valid']
    df_valid['error_abs'] = abs(df_valid['error'])

    print('---')
    print('Overal metrics...')

    # MAE
    print('MAE: {} m'.format(metrics.mean_absolute_error(df_valid['y_valid'],df_valid['y_predict'])))
    # R2
    print('R2: {}'.format(metrics.r2_score(df_valid['y_valid'],df_valid['y_predict'])))
    # RMSE 
    print('RMSE: {} m'.format(np.sqrt(metrics.mean_squared_error(df_valid['y_valid'],df_valid['y_predict']))))

    
    if city_counts != None:

        city_results = df_valid.groupby('city').apply(city_metrics).reset_index()

        # add building counts
        city_results = city_results.merge(city_counts,how='left',left_on='city',right_on='city')
    
        # print best and worst performing cities
        print(city_results[['city','MAE']].sort_values('MAE').head())
        print(city_results[['city','MAE']].sort_values('MAE').tail())
        print(city_results[['city','R2']].sort_values('R2').head())
        print(city_results[['city','R2']].sort_values('R2').tail())

        ## Aggregate by city size
        print('---')
        print('Aggregated by city...')
    
        # choose ranges
        ranges = [0,5000,20000,50000,np.inf]
    
        # add city counts to long result df
        df_valid = df_valid.merge(city_counts,how='left',left_on='city',right_on='city')
    
        # return aggregated MAE
        city_groups = df_valid.groupby(pd.cut(df_valid['count'], ranges))['error_abs'].mean()
        print(city_groups)


    ## Recall by height groups

    print('---')
    print('Recall by height group...')

    # choose range
    ranges = [0,5,10,15,np.inf]

    # mark (in)correctly predicted buildings - within 2m 
    df_valid['within_2'] = df_valid['error_abs'] < 2

    # mark (in)correctly predicted buildings - within 1m 
    df_valid['within_1'] = df_valid['error_abs'] < 1


    # Within 2 meters
    height_groups_2 = df_valid.groupby(pd.cut(df_valid['y_valid'], ranges))['within_2'].apply(lambda x: x.value_counts(normalize=True))
    print(height_groups_2)

    # Within 1 meter
    height_groups_1 = df_valid.groupby(pd.cut(df_valid['y_valid'], ranges))['within_1'].apply(lambda x: x.value_counts(normalize=True))
    print(height_groups_1)

    height_groups_1 = pd.DataFrame(height_groups_1)
    height_groups_1 = height_groups_1.unstack(level=1)
    height_groups_1.columns = ['False','True']
    height_groups_1 = height_groups_1['True']

    height_groups_2 = pd.DataFrame(height_groups_2)
    height_groups_2 = height_groups_2.unstack(level=1)
    height_groups_2.columns = ['False','True']
    height_groups_2 = height_groups_2['True']
    
    if city_counts != None:
        return(df_valid,city_results,height_groups_1,height_groups_2)

    else:
        return(df_valid,height_groups_1,height_groups_2)



def plot_two_height_distributions(distr1,distr2,label1,label2,experiment_name):
    '''
    Plots two heights distributions taking as inputs two DataFrame columns and respective labels.

    '''

    fig, ax = plt.subplots()

    sns.distplot(distr1,ax=ax,
        hist=True,kde=False,bins=range(0,25,1),hist_kws=dict(edgecolor="k", linewidth=1),
        label=label1)

    sns.distplot(distr2, ax=ax,
        hist=True,kde=False,bins=range(0,25,1),hist_kws=dict(edgecolor="k", linewidth=1),
        label=label2)
    plt.xlim(0, 25)
    ax.legend()
    plt.title('Heigh distributions - {}'.format(experiment_name))


    return(fig,ax)


def plot_learning_error(data1,data2,experiment_name):
    '''
    Plots the learning error for a given number of errors on the x_axis and two DataFrame columns.

    '''
    epochs = len(data1)
    x_axis = range(0, epochs)

    fig, ax = plt.subplots()

    ax.plot(x_axis, data1, label='Train')
    ax.plot(x_axis, data2, label='Test')
    ax.legend()
    plt.ylabel('error')
    plt.xlabel('epochs')
    plt.title('Learning curve - {}'.format(experiment_name))

    return(fig,ax)


def plot_joint_height_distr(df_valid,experiment_name,gridsize=50):
    '''
    Joint plot of the predicted and target height distributions.
    '''
    fig = plt.figure(figsize=(10,10))

    g = sns.JointGrid(df_valid.y_predict, df_valid.y_valid,xlim=(0, 20), ylim=(0, 20))
    
    g.plot_joint(plt.hexbin,  cmap="Purples", 
                 gridsize=gridsize, 
                 extent=[0, np.max(df_valid.y_predict), 0, np.max(df_valid.y_predict)]
                 )

    g.ax_joint.plot([0,20], [0,20], color = 'grey', linewidth=1.5)

    _ = g.ax_marg_x.hist(df_valid.y_predict, color="tab:purple", alpha=.6,
                           bins=np.arange(0, 20, 1))

    _ = g.ax_marg_y.hist(df_valid.y_valid, color="tab:purple", alpha=.6,
                           orientation="horizontal",
                           bins=np.arange(0, 20, 1))

    g.set_axis_labels('Predicted values in meters','Target values in meters')

    plt.title('Joint height distributions - {}'.format(experiment_name))

    plt.xticks(np.arange(0, 22, 2))
    plt.yticks(np.arange(0, 22, 2))

    return(fig)


def plot_cum_errors(df_valid,experiment_name):
    '''
    Outputs a plot of the cumulative prediction errors.
    '''
    fig, ax = plt.subplots(1, 1, figsize=(10,8))

    plt.hist(df_valid['error_abs'],cumulative=True,histtype='step',bins=1000,range=(0,10), density=True)

    plt.title('Cumulative distribution errors -- {}'.format(experiment_name))

    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(15)

    return(fig,ax)
    

def plot_error_distribution(df_valid,experiment_name):
    '''
    Outputs a plot of the error distribution.
    '''

    fig, ax = plt.subplots(figsize=(15,10))

    sns.distplot(df_valid['error'],
        hist=True,
        norm_hist=True,
        kde=False,
        bins=np.linspace(-10,10,41),
        hist_kws=dict(edgecolor="k", 
        linewidth=2),
        color = "tab:purple"
        )

    plt.xticks(np.arange(-10,11, 1.0))

    plt.title('Distribution errors -- {}'.format(experiment_name))

    return(fig,ax)



def violion_plot_errors(df_valid,experiment_name):
    '''
    Outputs a violin plot of the errors by height range.
    '''

    # add ranges to df 
    conds = [df_valid.y_valid.between(0,4.99),df_valid.y_valid.between(5,9.99),df_valid.y_valid.between(10,14.99),df_valid.y_valid.between(15,np.inf)]
    choices = ['(0,5]','(5,10]','(10,15]','(15,inf[']
    df_valid['range'] = np.select(conds,choices,4)

    # plot
    fig, ax = plt.subplots(1, 1, figsize=(10,8))
    ax = sns.violinplot(x="range", y="error", data=df_valid,
                   order=['(0,5]','(5,10]','(10,15]','(15,inf['],
                   palette='coolwarm',
                   linewidth=1.5)

    plt.ylim(-10, 10)

    plt.title('Errors distr. by height range -- {}'.format(experiment_name))

    return(fig,ax)


def get_ft_importance_sklearn(model,metrics_df=None):
    '''
    '''
    importance = list(model.feature_importances_)

    ft_names = [item for sublist in features_groups.values() for item in sublist]

    ft_importance = pd.DataFrame.from_dict(dict(zip(ft_names, importance)), orient = 'index')

    for ft_group in features_groups:

        print(ft_group)
        fts = ft_importance[ft_importance.index.isin(features_groups[ft_group])]
        ft_grp_av_imp = fts[0].sum() / len(features_groups[ft_group])
        print(ft_grp_av_imp)
        
        if metrics_df != None:
            metrics_df[ft_group] = ft_grp_av_imp
    
    if metrics_df != None: 
        return metrics_df


def get_ft_importance_xgb(model,metrics_df=None):
    '''
    '''
    ft_importance = pd.DataFrame.from_dict(model.get_booster().get_score(importance_type="gain"), columns=['importance'],orient='index')
    ft_importance['feature'] = ft_importance.index

    # print(OrderedDict(sorted(model.get_booster().get_score(importance_type="gain").items(), key=lambda t: t[1], reverse=True)))

    for ft_group in features_groups:

        print(ft_group)
        fts = ft_importance[ft_importance['feature'].isin(features_groups[ft_group])]
        ft_grp_av_imp = fts['importance'].sum() / len(features_groups[ft_group])
        print(ft_grp_av_imp)
        
        if metrics_df != None:
            metrics_df[ft_group] = ft_grp_av_imp
    
    if metrics_df != None: 
        return metrics_df